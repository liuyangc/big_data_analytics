## Notes

### Input data

`out.subelj_jdk`: The **adjacency matrix** of the network, space separated values, one edge per line

- First column: ID of from node 
- Second column: ID of to node
- **No Third column, edges are all unweighted**(can be seen at http://konect.uni-koblenz.de/networks/subelj_jdk)

### Desired output
- Experiment with at least 10 of the functions that in the lecture slides
- Use `simplify` function to reduce the complexity of graph since it is very large. But do more than simply using `simplify` function.
- Try some other functions in `igraph` package.
- Determine
    - the central node(s) in the graph
    - the longest path
    - the largest clique
    - betweenness centrality
    - power centrality
    and in each case, what does the data tell?
- question 7 TBA

### API
- `read.table` Reads a file in table format and creates a data frame from it

### Steps
- read `out.subelj_jdk_jdk` to get edges
- read `ent.subelj_jdk_jdk.class.names` to get nodes(class)
    ```r
    jdk_data <- read.table("~/Documents/GWU/Drive/S3/BigDataAnalytics/project1/subelj_jdk/out.subelj_jdk_jdk", header=FALSE, sep=" ", skip=2)
    nodes <- read.table("~/Documents/GWU/Drive/S3/BigDataAnalytics/project1/subelj_jdk/ent.subelj_jdk_jdk.class.name", header=FALSE, sep="")
    ```
- remove `N/A` column
    ```r
    edges <- subset(jdk_data, select=-c(V3))
    ```
- extract columns as vectors
    ```r
    #v1 <- edges$V1 
    #v2 <- edges$V2
    v1 <- as.character(nodes[edges$V1, 1])
    v2 <- as.character(nodes[edges$V2, 1])
    ```

- new relations data frame
    ```r
    relations <- data.frame(from=v1, to=v2)
    ```
- convert `data.frame` to graph by 
    ```r
    g <- graph.data.frame(relations, directed=FALSE)
    plot(g)
    ```
### screenshots
![test graph](./img/project1_1.png)
